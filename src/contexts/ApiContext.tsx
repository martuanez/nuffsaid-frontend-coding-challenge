import React, { createContext, useContext } from "react";

import Api from "../api";
import { IMessage } from "../types/IMessage";

export type ApiContextProps = {
  messagesApi: Api | null;
  clearMessages: () => void;
  clearMessage: (message: IMessage) => void;
};

const ApiContext = createContext<ApiContextProps>({
  messagesApi: null,
  clearMessages: () => null,
  clearMessage: () => null,
});

const ApiProvider: React.FC<ApiContextProps> = ({ children, ...props }) => (
  <ApiContext.Provider value={props}>{children}</ApiContext.Provider>
);

const useApiContext = (): ApiContextProps => useContext(ApiContext);

export { ApiContext, ApiProvider, useApiContext };
