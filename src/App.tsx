import { Grid } from "@material-ui/core";
import React from "react";
import styled from "styled-components";

import ApiActionButtons from "./components/ApiActionButtons";
import ErrorSnackbar from "./components/ErrorSnackbar";
import Header from "./components/Header";
import MessageList from "./components/MessageList";
import { ApiProvider } from "./contexts/ApiContext";
import useMessagesApi from "./hooks/useMessagesApi";
import useMessagesHandler from "./hooks/useMessagesHandler";

const MessagesWrapper = styled(Grid)`
  max-width: 1400px;
`;

const App: React.FC = () => {
  const {
    errorMessage,
    errorMessages,
    infoMessages,
    warningMessages,
    messagesCallback,
    clearMessages,
    clearMessage,
  } = useMessagesHandler();
  const messagesApi = useMessagesApi(messagesCallback);

  return (
    <ApiProvider
      messagesApi={messagesApi}
      clearMessages={clearMessages}
      clearMessage={clearMessage}
    >
      <Header />
      <ErrorSnackbar errorMessage={errorMessage} />
      <ApiActionButtons />
      <Grid container justify="center">
        <MessagesWrapper
          container
          item
          direction="row"
          justify="space-evenly"
          spacing={1}
        >
          <MessageList title="Error Type 1" messages={errorMessages} />
          <MessageList title="Warning Type 2" messages={warningMessages} />
          <MessageList title="Info Type 3" messages={infoMessages} />
        </MessagesWrapper>
      </Grid>
    </ApiProvider>
  );
};

export default App;
