import { useState } from "react";

import { IMessage, Priority } from "../types";

interface IMessagesHandler {
  messagesCallback: (message: IMessage) => void;
  errorMessages: IMessage[];
  warningMessages: IMessage[];
  infoMessages: IMessage[];
  errorMessage: IMessage | null;
  clearMessages: () => void;
  clearMessage: (message: IMessage) => void;
}

const useMessagesHandler = (): IMessagesHandler => {
  const [errorMessages, setErrorMessages] = useState<IMessage[]>(() => []);
  const [warningMessages, setWarningMessages] = useState<IMessage[]>(() => []);
  const [infoMessages, setInfoMessages] = useState<IMessage[]>(() => []);

  const [errorMessage, setErrorMessage] = useState<IMessage | null>(null);

  const messagesCallback = (message: IMessage) => {
    if (message.priority === Priority.error) {
      setErrorMessage(message);
      setErrorMessages((prevState) => [...prevState, message]);
    }

    if (message.priority === Priority.warning) {
      setWarningMessages((prevState) => [...prevState, message]);
    }

    if (message.priority === Priority.info) {
      setInfoMessages((prevState) => [...prevState, message]);
    }
  };

  const clearMessages = () => {
    setErrorMessage(null);
    setErrorMessages([]);
    setWarningMessages([]);
    setInfoMessages([]);
  };

  const clearMessage = (message: IMessage) => {
    if (message.priority === Priority.error) {
      if (message === errorMessage) {
        setErrorMessage(null);
      }
      setErrorMessages(errorMessages.filter((msg) => msg !== message));
    }

    if (message.priority === Priority.warning) {
      setWarningMessages(warningMessages.filter((msg) => msg !== message));
    }

    if (message.priority === Priority.info) {
      setInfoMessages(infoMessages.filter((msg) => msg !== message));
    }
  };

  return {
    messagesCallback,
    errorMessages,
    warningMessages,
    infoMessages,
    errorMessage,
    clearMessages,
    clearMessage,
  };
};

export default useMessagesHandler;
