import { useEffect, useState } from "react";

import Api from "../api";
import { IMessage } from "../types";

const useMessagesApi = (
  messageCallback: (message: IMessage) => void
): Api | null => {
  const [api, setApi] = useState<Api>();

  useEffect(() => {
    if (!api) {
      const newApi = new Api({ messageCallback });

      setApi(newApi);
    } else {
      api.start();
    }
  }, [api]);

  return api ?? null;
};

export default useMessagesApi;
