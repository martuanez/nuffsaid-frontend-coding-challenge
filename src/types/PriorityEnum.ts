export enum Priority {
  error = 1,
  warning = 2,
  info = 3,
}
