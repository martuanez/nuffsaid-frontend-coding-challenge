export interface IMessage {
  message: string;
  priority: 1 | 2 | 3;
}
