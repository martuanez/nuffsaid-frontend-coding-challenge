import { Grid, Typography } from "@material-ui/core";
import React from "react";
import styled from "styled-components";

import { IMessage } from "../types/IMessage";

import MessageCard from "./MessageCard";

interface MessageListProps {
  title: string;
  messages: IMessage[];
}

const MessageListWrapper = styled.div`
  width: 30%;
`;

const MessageList: React.FC<MessageListProps> = ({ title, messages }) => (
  <MessageListWrapper>
    <Typography variant="h6" component="h2">
      {title}
    </Typography>
    <Typography variant="subtitle1" gutterBottom>
      Count {messages.length}
    </Typography>
    <Grid direction="column-reverse" spacing={1}>
      {messages.reverse().map((message) => (
        <MessageCard key={message.message} message={message} />
      ))}
    </Grid>
  </MessageListWrapper>
);

export default MessageList;
