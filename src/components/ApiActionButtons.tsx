import { Grid } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import React, { useEffect, useState } from "react";
import styled from "styled-components";

import { useApiContext } from "../contexts/ApiContext";

const StyledGrid = styled(Grid)`
  margin-bottom: 32px;
`;
const StyledButton = styled(Button)`
  margin: 0 8px;
  background-color: #88fca3;
`;

const ApiActionButtons: React.FC = () => {
  const { messagesApi, clearMessages } = useApiContext();
  const [isApiStarted, setIsApiStarted] = useState(false);

  useEffect(() => {
    setIsApiStarted(messagesApi?.isStarted() ?? false);
  }, [messagesApi?.isStarted()]);

  const updateApiState = () =>
    setIsApiStarted(messagesApi?.isStarted() ?? false);

  const handleStartStopButtonClick = () => {
    if (isApiStarted) {
      messagesApi?.stop();
    } else {
      messagesApi?.start();
    }
    updateApiState();
  };

  return (
    <StyledGrid container direction="row" justify="center" alignItems="center">
      <StyledButton variant="contained" onClick={handleStartStopButtonClick}>
        {isApiStarted ? "Stop" : "Start"}
      </StyledButton>
      <StyledButton variant="contained" onClick={clearMessages}>
        Clear
      </StyledButton>
    </StyledGrid>
  );
};

export default ApiActionButtons;
