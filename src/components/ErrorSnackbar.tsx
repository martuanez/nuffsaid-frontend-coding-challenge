import { Snackbar } from "@material-ui/core";
import Alert from "@material-ui/lab/Alert";
import React, { useEffect, useState } from "react";

import { IMessage } from "../types/IMessage";

interface ErrorSnackbarProps {
  errorMessage: IMessage | null;
}

const ErrorSnackbar: React.FC<ErrorSnackbarProps> = ({ errorMessage }) => {
  const [currentMessage, setCurrentMessage] = useState<string>();

  useEffect(() => {
    if (errorMessage && errorMessage.message !== currentMessage) {
      setCurrentMessage(errorMessage.message);
    }
  }, [errorMessage?.message]);

  const clearErrorMessage = () => setCurrentMessage("");

  return currentMessage ? (
    <Snackbar
      open={errorMessage?.message === currentMessage}
      autoHideDuration={2000}
      onClose={clearErrorMessage}
      anchorOrigin={{ vertical: "top", horizontal: "center" }}
    >
      <Alert onClose={clearErrorMessage} severity="error" variant="filled">
        {currentMessage}
      </Alert>
    </Snackbar>
  ) : null;
};

export default ErrorSnackbar;
