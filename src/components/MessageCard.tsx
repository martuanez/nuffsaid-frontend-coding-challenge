import { Paper, Button } from "@material-ui/core";
import React from "react";
import styled from "styled-components";

import { useApiContext } from "../contexts/ApiContext";
import { IMessage } from "../types/IMessage";
import { Priority } from "../types/PriorityEnum";

interface MessageCardProps {
  message: IMessage;
}

const getPriorityColor = (priority: Priority): string => {
  if (priority === Priority.error) return "#F56236";
  if (priority === Priority.warning) return "#FCE788";
  return "#88FCA3";
};

const StyledPaper = styled(Paper)<{ priority: Priority }>`
  background: ${(props) => getPriorityColor(props.priority)};
  border-radius: 3px;
  margin-bottom: 12px;
  padding: 12px 8px 8px 12px;
  height: 84px;
  display: flex;
  justify-content: space-between;
  flex-direction: column;
`;

const StyledButtonRow = styled.div`
  text-align: right;
  margin-top: 24px;
`;

const MessageCard: React.FC<MessageCardProps> = ({ message }) => {
  const { clearMessage } = useApiContext();

  return (
    <StyledPaper priority={message.priority}>
      <div>{message.message}</div>
      <StyledButtonRow>
        <Button onClick={() => clearMessage(message)}>Clear</Button>
      </StyledButtonRow>
    </StyledPaper>
  );
};

export default MessageCard;
