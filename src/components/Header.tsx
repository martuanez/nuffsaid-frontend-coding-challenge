import { Typography } from "@material-ui/core";
import React from "react";
import styled from "styled-components";

const StyledDiv = styled.div`
  margin-bottom: 16px;
  padding: 16px;
  border-bottom: 1px solid black;
`;

const Header: React.FC = () => (
  <StyledDiv>
    <Typography variant="h4" component="h1">
      nuffsaid.com Coding Challenge
    </Typography>
  </StyledDiv>
);

export default Header;
